package at.zie.observerPattern;

public interface Observable {
	public void inform();
}
